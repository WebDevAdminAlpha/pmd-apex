package plugin

import (
	"bufio"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks the filename for sfdx-project.json or if the file contents
// contain an apex class definition
func Match(path string, info os.FileInfo) (bool, error) {
	switch name := info.Name(); name {
	case "sfdx-project.json":
		return true, nil
	case "package.xml":
		f, err := os.Open(filepath.Clean(path))
		if err != nil {
			return false, err
		}

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			// Apex detection relies on an ApexClass definition
			if strings.TrimSpace(scanner.Text()) == "<name>ApexClass</name>" {
				return true, nil
			}
		}
		if err := f.Close(); err != nil {
			return false, err
		}
	}

	return false, nil
}

func init() {
	plugin.Register("pmd-apex", Match)
}
